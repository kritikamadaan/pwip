from django.db import models

# Create your models here.
from django.db import models
from django.utils.timezone import now
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import get_user_model
User = get_user_model()
from product.models import Product
from django.contrib.postgres.fields import ArrayField, JSONField

class Small_Order(models.Model):
    BOOL_CHOICES = (
        ('Yes', 'Yes'),
        ('No', 'No')
    )    
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product_name = JSONField(default=dict)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, default="0.0")
    isDelivered = models.CharField(max_length=20, choices=BOOL_CHOICES, default="No")
    ordered_date_time = models.DateTimeField(default=now)
    payment_date_time = models.DateTimeField(null=True)
    paymentDone = models.CharField(max_length=20, choices=BOOL_CHOICES, default="No")
    ordered_users_phone = models.CharField(_("Ordered User's Phone"), max_length=50, default="None")
    ordered_users_countryCode = models.CharField(_("Ordered User's Country Code"),max_length=12, default="None", blank=False)
    ordered_users_zip_code = models.CharField(_("Ordered User's Zip/Pin Code"), max_length=100, default="None")
    order_id = models.CharField(_("Order ID"), max_length=200, null=False, blank=False, default="None")

    def __str__(self):
        return f'{self.user} Ordered "{self.product_name}" With Order Id  "{self.order_id}"'


class Add_to_Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    qty = models.IntegerField(default="1")
    price = models.DecimalField(max_digits=10, decimal_places=2, default="0.0")
    ordered_at = models.DateTimeField(default=now)

    def __str__(self):
        return f'{self.user} ordered {self.product} at {self.ordered_at}'


class Bulk_orders(models.Model):
    phone = models.CharField(_("Phone"), max_length=12, null=False)
    email = models.EmailField(_("Email"), max_length=254)
    details = models.TextField(_("Queries/Details"), max_length=1000, blank=True)
