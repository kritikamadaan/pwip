from django.contrib import admin
from .models import Small_Order, Add_to_Cart

# Register your models here.
admin.site.register(Small_Order)
admin.site.register(Add_to_Cart)
