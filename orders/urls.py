from django.conf.urls import url
from django.urls import path, include, reverse_lazy
from .import views

urlpatterns = [
    url(r'^order_create/$', views.OrderCreateApiView.as_view()),
    url(r'^order_confirmation/(?P<ordid>[\w\-]+)/$', views.OrderPaymentConfirmation.as_view()),
    url(r'^order_list/$', views.OrderListApiView.as_view()),
    url(r'^OrderUpdateDeleteDetailApiView/(?P<id>[0-9]+)$', views.OrderUpdateDeleteDetailApiView.as_view()),
    url(r'^cart_create_list/$', views.Add_to_Cart_ApiView.as_view()),
    url(r'^user_cart/(?P<id>[0-9]+)$', views.Cart_of_A_User_ApiView.as_view()),
    url(r'^bulk_orders/$', views.BulkOrderApiView.as_view()),
]
