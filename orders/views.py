from django.db.models import Q
from .serializers import (
    OrderSerializer,
    OrderDetailListSerializer,
    Add_to_Cart_Serializer,
    BulkOrderSerializer
)
from .models import (
    Small_Order,
    Add_to_Cart,
    Bulk_orders
)
from product.models import Product
from rest_framework.views import APIView
from rest_framework.generics import (
    CreateAPIView,
    ListCreateAPIView,
    ListAPIView,
    RetrieveDestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.permissions import AllowAny
from users.views import send_sms
from product.models import Product
from django.contrib.auth import get_user_model
User = get_user_model()
from django.utils.crypto import get_random_string
from django.utils import timezone
import pytz
from rest_framework.parsers import JSONParser,ParseError
from django.conf import settings
from django.core.mail import send_mail
from operator import itemgetter

# Create your views here

class OrderCreateApiView(APIView):
    # permission_classes = [AllowAny]
    # serializer_class = OrderSerializer
    # queryset = Small_Order.objects.all()

    def post(self, request):
        try:
            j_form = request.data
            # print(j_form['product_name'])

            # ordered user details
            ordered_user = User.objects.get(id = request.user.id)
            ordered_user_phone = ordered_user.phone
            ordered_user_Country_Code = ordered_user.country
            zcode = ordered_user.zip_code
            or_id = get_random_string(length=32)

            # Parse Products
            prod_lst = []

            product_name_list = j_form['product_name']
            product_id_list = j_form['product_id']
            product_qty_list = j_form['qty_no']

            try:
                len(product_qty_list) or len(product_id_list)  != len(product_name_list)
                # print("id", len(product_id_list))
                # print("name", len(product_name_list))
                # print("qty", len(product_qty_list))
                pass
            except Exception as e:
                print("id", len(product_id_list))
                print("name", len(product_name_list))
                print("qty", len(product_qty_list))                
                return Response({"Error":str(e)})

            for i in range(len(product_name_list)):
                a={}
                a['Name'] = product_name_list[i]
                a['Id'] = product_id_list[i]
                a['qty'] = product_qty_list[i]
                prod_lst.append(a)
            print("before storing")
            order = Small_Order.objects.create(
                user = request.user,
                product_name = prod_lst,
                total_price = j_form['total_price'],
                ordered_users_phone = ordered_user.phone,
                ordered_users_countryCode = ordered_user.country,
                order_id = or_id,
                ordered_users_zip_code = zcode,
            )
            
            return Response({"Order":"Placed Successfully!", "User's Order id":or_id}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST)

class OrderPaymentConfirmation(APIView):
    serializer_class = OrderSerializer
    permission_classes = [AllowAny]

    def post(self, request, ordid=None): # id = order id from
        try:
            user_order_id = ordid
            order_details = Small_Order.objects.get(order_id=user_order_id)
            order_details.paymentDone = "Yes"
            order_details.payment_date_time = timezone.now()

            o = Small_Order.objects.filter(order_id=user_order_id).get()

            res = list(map(itemgetter('Name'), o.product_name))
            prd_name = (', '.join(res))
            prd_qty = list(map(itemgetter('qty'), o.product_name))

            prd_details = []
            for i in o.product_name:
                a = f'{i["Name"]}({i["qty"]})'
                prd_details.append(a)
            parsed_details = (', '.join(prd_details))
            # print(parsed_details)

            # Msg users about the payment confirmation and product details
            ordered_user_no = (order_details.ordered_users_countryCode + order_details.ordered_users_phone).replace('+','')
            user_msg = "Payment for the Order(ID) '" + order_details.order_id + "' is confirmed. You will recieve your order soon." + "\n\nOrder Details:\nProduct(s) Name - " + parsed_details + "\nTotal Price " + str(order_details.total_price)
            send_sms(number = ordered_user_no, msg = user_msg)
            # print(user_msg)

            # Mail Company about the orders
            comp_msg = "You have received the following order\n\nProduct(s) Name - " + parsed_details + "\nTotal Price - " + str(order_details.total_price) + ". \n\nUser Details:\nName/Email - " + str(order_details.user) + "\nPhone - " + str(ordered_user_no) + "\nZip Code - " + str(order_details.ordered_users_zip_code)
            send_mail('Order Recieved', comp_msg, settings.DEFAULT_FROM_EMAIL, ['shreehari9481@gmail.com'], fail_silently=False)
            print(comp_msg)

            order_details.save()

            return Response({"Payment":order_details.paymentDone, "User's Order id":user_order_id, "Payment Timings":order_details.payment_date_time}, status=HTTP_200_OK)
        except Exception as e:
            print(">>>>", e)
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST)

class OrderListApiView(ListAPIView):
    queryset = Small_Order.objects.all()
    serializer_class = OrderDetailListSerializer

class OrderUpdateDeleteDetailApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = OrderDetailListSerializer
    queryset = Small_Order.objects.all()
    lookup_field = 'id'

class Add_to_Cart_ApiView(ListCreateAPIView):
    serializer_class = Add_to_Cart_Serializer
    queryset = Add_to_Cart.objects.all()
    # permission_classes = [AllowAny]

class Cart_of_A_User_ApiView(APIView):
    # permission_classes = [AllowAny]
    def get(self, request, id=None):
        try:
            result = Add_to_Cart.objects.filter(user=id)
            return Response({"Cart": Add_to_Cart_Serializer(result, many=True).data}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST)

class BulkOrderApiView(APIView):
    
    def post(self, request):
        try:
            phone = request.data['phone'].replace("+", "")
            email = request.data['email']
            queries = request.data['queries']
            # print(email)
            # send mail to pwip
            msg = "You have recieved a bulk order. \nDetails:\n Phone - " + phone + "\n Email - " + email + "\n Details - " + queries
            send_mail('BULK ORDER', msg, settings.DEFAULT_FROM_EMAIL, ['shreehari9481@gmail.com'], fail_silently=False)
            return Response({"Bulk":"Placed Successfully!"}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST)
