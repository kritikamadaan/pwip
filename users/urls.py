from django.conf.urls import url
from django.urls import path, include, reverse_lazy
from .import views



urlpatterns = [
    path("",views.index,name="BlogHome"),
    url(r'^register/$', views.UserAuthAPiView.as_view()),
    url(r'^user_list/$', views.UserListsApiView.as_view()),
    url(r'^user_detail/(?P<id>[0-9]+)$', views.UserDetailApiView.as_view()),
    url(r'^login_snd_code/$', views.UserLoginPhoneSendApiView.as_view()),
    url(r'^login_verify_code/$', views.UserPhoneVerificationVerifyApiView.as_view()),
    url(r'^business_type/$', views.BusinessApiView.as_view()),
    url(r'^chatwithus/$', views.ChatWithUsApiView.as_view()),
    url(r'^user_detail/(?P<id>[0-9]+)$', views.UserDetailApiView.as_view()),
    url(r'^send_query/$', views.UserQueryApiView.as_view()),
    url(r'^user_subs/$', views.UserSubscriptionApiView.as_view()),
    url(r'^userlist/$', views.listsubs.as_view()),
    url(r'^user_subs_update/(?P<user_id>[0-9]+)/$', views.UserSubscriptionUpdateDestroyApiView.as_view()),
    url(r'^subs_amount/$', views.SubscriptionAmountApiView.as_view()),
]