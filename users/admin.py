from django.contrib import admin
from .models import User, Business_Type, OTPModel, ChatWithUs, UserSubscriptionModel, SubscriptionAmount
from django.contrib.auth.models import Group

# Register your models here.
# register your model here
class ChatWithUsAdmin(admin.ModelAdmin):
        def has_add_permission(self, request):
                return False
        
        def has_delete_permission(self, request):
                return False

class SubscriptAmountAdmin(admin.ModelAdmin):
        def has_add_permission(self, request):
                return False
        
        def has_delete_permission(self, request):
                return False


admin.site.register(User)
#admin.site.register(Product_Interest)
admin.site.register(Business_Type)
admin.site.unregister(Group)
admin.site.register(OTPModel)
admin.site.register(ChatWithUs, ChatWithUsAdmin)
admin.site.register(UserSubscriptionModel)
admin.site.register(SubscriptionAmount, SubscriptAmountAdmin)
