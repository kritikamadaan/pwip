from rest_framework.serializers import ModelSerializer, Serializer
from .models import (
    User, 
    Business_Type, 
    #Product_Interest,
    ChatWithUs,
    UserQueriesModel, 
    UserSubscriptionModel,
    SubscriptionAmount
    )
from rest_framework  import serializers



# serialize function are the format to serialize the data in json
# Serializers
class UserCreateSerializer(ModelSerializer):     
    class Meta:
        model=User
        fields = [
            'full_name',
            'email',
            'companyName',
            'business_type',
            'gstNumber',
            'prod_interest',
            'country',
            'zip_code',
            'phone',
            'role',
        ]


class UserDetailSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class UserLoginPhoneVerficationSendSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            "country",
            "phone",
        ]


class UserLoginPhoneVerficationVerifySerializer(ModelSerializer):
    code = serializers.CharField()
    class Meta:
        model = User
        fields = [
            "country",
            "phone",
            "code"
        ]

class BusinessTypeSerializer(ModelSerializer):
    class Meta:
        model = Business_Type
        fields = '__all__'


class ChatWithUsSerializer(ModelSerializer):
    class Meta:
        model = ChatWithUs
        fields = '__all__'



class UserQueriesSerializer(Serializer):

    class Meta:
        model = UserQueriesModel
        fiels = '__all__'    

class UserSubscriptionSerializer(ModelSerializer):

    class Meta:
        model = UserSubscriptionModel
        fields = '__all__'

class SubscriptionAmountSerializer(ModelSerializer):

    class Meta:
        model = SubscriptionAmount
        fields = '__all__'


