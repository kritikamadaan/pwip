from django.contrib.auth.models import AbstractUser, BaseUserManager ## A new class is imported. ##
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.timezone import now
from datetime import timedelta
import pytz

# Create your models here.
class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username,email, password, **extra_fields)


class User(AbstractUser):
    BOOLEAN_CHOICES = (
        (True, 'Yes'),
        (False, 'No')
    )
    """User model."""
    username = None
    full_name = models.CharField(_("Full Name"), max_length=200) # cannot use username, as we are overriding username for user authentication with phone
    email = models.EmailField(_('email address'), unique=True)
    companyName = models.CharField(_("Company/Firm Name"), max_length=500)
    business_type = models.CharField(_("Type of Business"), max_length=500)
    gstNumber = models.CharField(_("Business REG. NO./GST NO."), max_length=200)
    prod_interest = models.CharField(_("Product of Interest"), max_length=300)
    country = models.CharField(_("Country(with code)"), max_length=50)
    zip_code = models.CharField(_("Zip/Pin Code"), max_length=100)
    phone = models.CharField(max_length=12, null=False, unique=True)
    user_verified =  models.BooleanField(_("User Verified Through Phone number"), choices=BOOLEAN_CHOICES, default=False)
    role = models.CharField(_("Role"), max_length=50, default="miller")
    user_registered = models.DateTimeField(auto_now_add=True)

    USERNAME_FIELD = 'email'      #a field that is uniquely identified in model.
    REQUIRED_FIELDS = []          # field which is required

    objects = UserManager() ## This is the new line in the User model. ##



class Business_Type(models.Model):
    businness_type_name = models.CharField(_("Type of Business"), max_length=50)

    def __str__(self):
        return self.businness_type_name


class OTPModel(models.Model):
    BOOLEAN_CHOICES = (
        (True, 'Yes'),
        (False, 'No')
    )    
    phone = models.CharField(_("Phone"), max_length=50)
    countryCode = models.CharField(_("Country Code"),max_length=12, default="None", blank=False)
    otp = models.CharField(_("OTP"), max_length=100, null=True)
    used = models.BooleanField(_("Is OTP used?"), choices=BOOLEAN_CHOICES, default=False)

    def __str__(self):
        return f'{self.countryCode} {self.phone}'


class ChatWithUs(models.Model):
    countryCode = models.CharField(_("Country Code"),max_length=12, default="+91", blank=False)
    phone = models.CharField(_("Phone"), max_length=200)

    class Meta:
        verbose_name = 'ChatWithUs'
        verbose_name_plural = 'ChatWithUs'
    
    def __str__(self):
        return f'{self.countryCode} {self.phone}'    


class UserQueriesModel(models.Model):
    phone = models.CharField(_("Phone"), max_length=12, null=False)
    email = models.EmailField(_("Email"), max_length=254)
    details = models.TextField(_("Queries/Details"), max_length=1000, blank=True)    


class UserSubscriptionModel(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    subs_start_date = models.DateTimeField(default=now)
    subs_end_date = models.DateTimeField(default=now)

    class Meta:
        verbose_name = _("User Subscription")    
        verbose_name_plural = _("User Subscription")

    def __str__(self):
        return f'{self.user} Subscribed on {self.subs_start_date.strftime("%d/%m/%Y:%H:%M:%S")} And Ends on {self.subs_end_date.strftime("%d/%m/%Y:%H:%M:%S")}'



class SubscriptionAmount(models.Model):
    subsamount = models.PositiveIntegerField(_("Subscription Amount"))

    class Meta:
        verbose_name = _("Subscription Amount")    
        verbose_name_plural = _("Subscription Amount")

    def __str__(self):
        return f'Subscription Amount : {self.subsamount}'
    
