from django.shortcuts import render
from django.http import HttpResponse

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
#imports
from .models import (
    User,
    OTPModel,
    Business_Type, 
    #Product_Interest,
    ChatWithUs,
    UserQueriesModel, 
    UserSubscriptionModel,
    SubscriptionAmount
) 
from .serializers import (
    UserCreateSerializer, 
    UserDetailSerializer,
    UserLoginPhoneVerficationSendSerializer,
    UserLoginPhoneVerficationVerifySerializer,
    BusinessTypeSerializer,
    #ProductInterestSerializer,
    ChatWithUsSerializer,
    UserQueriesSerializer,
    UserSubscriptionSerializer,
    SubscriptionAmountSerializer
) 
from django.contrib.auth import get_user_model
User = get_user_model()
from django.contrib.auth import logout, login
from rest_framework_jwt.settings import api_settings
JWT_PAYLOAD_HANDLER = api_settings.JWT_PAYLOAD_HANDLER
JWT_ENCODE_HANDLER = api_settings.JWT_ENCODE_HANDLER
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST, HTTP_403_FORBIDDEN
from django.conf import settings
import requests
import urllib
import urllib.request
from random import randint
from django.db import IntegrityError
import re
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.conf import settings
from django.core.mail import send_mail
from django.utils.timezone import now
from datetime import timedelta



# Create your views here.

#Create your views
class UserAuthAPiView(CreateAPIView):
    serializer_class = UserCreateSerializer
    permission_classes = [AllowAny]
    def post(self,request, *args, **kwargs):
        try:
            info = request.data
            phone_exists = User.objects.filter(phone=info['phone'])
            email_exists = User.objects.filter(email=info['email'])
            if phone_exists.get():
                print("User exists with number-", info['phone'])
                return Response({"status":"User with this phone number Already exists"})
        except User.DoesNotExist as register:
            user = User.objects.create(
                full_name = info['full_name'],
                email = info['email'],
                companyName = info['companyName'],
                business_type = info['business_type'],
                gstNumber = info['gstNumber'],
                prod_interest = info['prod_interest'],
                country = info['country'],
                zip_code = info['zip_code'],
                phone = info['phone'],
                role = info['role']
            )
            payload = JWT_PAYLOAD_HANDLER(user)
            jwt_token = JWT_ENCODE_HANDLER(payload)
            # print("User Id>>>", user.id)
            user_id = user.id
            subs_start = now()
            subs_end = (now()  + timedelta(days=15))
            user_subs = UserSubscriptionModel.objects.create(
                user_id = user_id,
                subs_start_date = subs_start,
                subs_end_date = subs_end
            )
            return Response({
                "status":"User Registration Done", 
                "user id":user.id,
                "email":user.email,
                "phone":user.phone, 
                "token":jwt_token,
                "Verified":user.user_verified,
                "Subscription Start Date":user_subs.subs_start_date.strftime("%d/%m/%Y:%H:%M:%S"),
                "Subscription End Date":user_subs.subs_end_date.strftime("%d/%m/%Y:%H:%M:%S")
            })
        except Exception as e:
            print(">>>>>", str(e))
            return Response({"Status":False,"msg":str(e)})


class UserListsApiView(ListAPIView):
    serializer_class = UserDetailSerializer   
    queryset = User.objects.all()

class UserDetailApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()
    lookup_field = 'id'


class UserLoginPhoneSendApiView(APIView):
    serializer_class = UserLoginPhoneVerficationSendSerializer
    permission_classes = [AllowAny]
    def post(self,request):
        try:
            send_to = str(request.data['phone'])
            country = str(request.data['country'])
            user_exists = User.objects.filter(phone=send_to).get()
            otp = randint(100000, 999999)
            # print("Country code>>>", country, "data type->", type(country))
            message = "Enter the verification code\n\n\t\t\t"+str(otp)
            OTPModel.objects.create(phone=send_to, otp=otp, countryCode=country)
            number = (country+send_to).replace('+','')
            # print("NUmber>>>>", number, "type>>>", type(number))
            send_sms(number, message)
            return Response({"msg":"user exists", "country":country, "phone":number, "OTP ":otp}, status=HTTP_200_OK)
        except User.DoesNotExist as ud:
            return Response({"Status":"User with phone number does not exist:"}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("Printing exception", str(e))
            return Response({"status":"Something went wrong. Please Make sure that user with this phone number is already registered"}, status=HTTP_400_BAD_REQUEST)


class UserPhoneVerificationVerifyApiView(APIView):
    serializer_class = UserLoginPhoneVerficationVerifySerializer
    permission_classes = [AllowAny]
    def post(self, request):
        try:
            print(request.data)
            phone = request.data['phone']
            code = request.data['code']
            country_code = request.data['country']
            otp_model = OTPModel.objects.filter(otp=code, phone=phone, countryCode=country_code).get()
            if otp_model and not otp_model.used :
                user = User.objects.filter(phone=phone).get()
                user.user_verified = True
                u_id = user.id
                user.last_login = now()
                user.save()
                otp_model.used = True
                otp_model.save()
                payload = JWT_PAYLOAD_HANDLER(user)
                jwt_token = JWT_ENCODE_HANDLER(payload)
                OTPModel.objects.filter(phone=phone).delete()
                u_subs = UserSubscriptionModel.objects.filter(user_id = u_id).first()
                subs_start = u_subs.subs_start_date
                end_date = u_subs.subs_end_date
                return Response({
                    "msg":"User is Sucessfully verified !",
                    "user_id":user.id,
                    "Verification":user.user_verified,
                    "phone":user.phone,
                    "token":jwt_token,
                    "Subscription Start Date":subs_start.strftime("%d/%m/%Y:%H:%M:%S"),
                    "Subscription End Date":end_date.strftime("%d/%m/%Y:%H:%M:%S")
                }, status=HTTP_200_OK) 
            else:
                return Response({"Message":"Oops! OTP is already Used"}, status=HTTP_400_BAD_REQUEST)
        except OTPModel.DoesNotExist as od:
            print(str(od))
            return Response({"Status":"Entered OTP and Phone number doesn't match"}, status=HTTP_400_BAD_REQUEST)
        except User.DoesNotExist as ud:
            return Response({"Status":"User with this phone number does not exist"}, status=HTTP_400_BAD_REQUEST)
        except Exception as e:
            print(str(e))
            return Response({"status":"User not verified. Check for Exception"}, status=HTTP_400_BAD_REQUEST)


class BusinessApiView(ListCreateAPIView):
    serializer_class = BusinessTypeSerializer
    queryset = Business_Type.objects.all()


class ChatWithUsApiView(APIView):
    def get(self, request):
        try:
            result = ChatWithUs.objects.first()
            msg = f'{result.countryCode} {result.phone}'
            return Response({"Phone":msg}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Msg":str(e)}, status=HTTP_400_BAD_REQUEST)


class UserQueryApiView(APIView):    
    def post(self, request):
        try:
            phone = request.data['phone'].replace("+", "")
            email = request.data['email']
            queries = request.data['queries']
            # print(email)
            # send mail to pwip
            msg = "You have recieved a Query from a user. \nDetails:\n Phone - " + phone + "\n Email - " + email + "\n Details - " + queries
            send_mail('QUERY', msg, settings.DEFAULT_FROM_EMAIL, ['shreehari@urbanstopstudio.co'], fail_silently=False)
            # print(msg)
            return Response({"Query":"Sent Successfully!"}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST) 


class UserSubscriptionApiView(APIView):

    def post(self, request):
        try:
            user_id = request.data['user']
            subs_user_exists = UserSubscriptionModel.objects.filter(user_id = user_id).first()
            # print("Subs Start {0} \nSubsc Start {1}".format(subs_user_exists.subs_start_date.strftime("%d/%m/%Y:%H:%M:%S"), subs_user_exists.subs_end_date.strftime("%d/%m/%Y:%H:%M:%S")))
            subs_start = now()
            subs_end = (subs_start + timedelta(days=60))
            subs_user_exists = UserSubscriptionModel.objects.filter(user_id = user_id).update(
                subs_start_date = subs_start,
                subs_end_date = subs_end
            )
            return Response({"Subscription":"Updated", "Subs_start_date":subs_start.strftime("%d/%m/%Y:%H:%M:%S"), "Subs_end_date":subs_end.strftime("%d/%m/%Y:%H:%M:%S")}, status=HTTP_200_OK)
        except UserSubscriptionModel.DoesNotExist as subs:
            subs_start = now()
            subs_end = (subs_start + timedelta(days=60))
            create_subs_user = UserSubscriptionModel.objects.create(
                user_id = request.user.id,
                subs_start_date = subs_start,
                subs_end_date = subs_end
            )  
            return Response({"Subscription":"Created", "Subs_start_date":subs_start.strftime("%d/%m/%Y:%H:%M:%S"), "Subs_end_date":subs_end.strftime("%d/%m/%Y:%H:%M:%S")}, status=HTTP_200_OK)      
        except Exception as e:
            return Response({"Error":str(e)}, status=HTTP_400_BAD_REQUEST)


class listsubs(ListAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserSubscriptionSerializer
    queryset = UserSubscriptionModel.objects.all()


class UserSubscriptionUpdateDestroyApiView(RetrieveUpdateDestroyAPIView):
    permission_classes = [AllowAny]
    serializer_class = UserSubscriptionSerializer
    queryset = UserSubscriptionModel.objects.all()
    lookup_field = 'user_id'


class SubscriptionAmountApiView(ListCreateAPIView):
    serializer_class = SubscriptionAmountSerializer
    queryset = SubscriptionAmount.objects.all()






def index(request):
    return HttpResponse("Index Blog ")
    


