from django.db import models
from django.utils.timezone import now
from django.contrib.auth import get_user_model
User = get_user_model()
# Create your models here.


class Region(models.Model):
    name = models.CharField(max_length=200)
    image = models.CharField(max_length=300, null=True, blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    STOCK_CHOICES = (
        ('In Stock', 'In Stock'),
        ('Out of Stock', 'Out of Stock'),
        ('Limited Stock', 'Limited Stock')
    )
    name = models.CharField(max_length=100)
    price = models.DecimalField(max_digits=10, decimal_places=2, default="0.0")
    qty = models.IntegerField(default="0")
    regionId = models.ForeignKey(Region, on_delete=models.CASCADE)
    description = models.TextField(max_length=500, default="")
    image1 = models.CharField(max_length=300, null=True, blank=True)
    image2 = models.CharField(max_length=300, null=True, blank=True)
    image3 = models.CharField(max_length=300, null=True, blank=True)
    inStock = models.CharField(max_length=20, choices=STOCK_CHOICES)
    created_date = models.DateTimeField(auto_now_add=True)
    last_updated_date = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False, default="1")

    def save(self, *args, **kwargs):
        super(Product, self).save(*args, **kwargs)
        Product_Price_History.objects.create(product_id_id=self.id, prices=self.price)

    def __str__(self):
        return self.name


class Product_Price_History(models.Model):
    product_id = models.ForeignKey(Product, on_delete=models.CASCADE)
    prices = models.DecimalField(max_digits=10, decimal_places=2, default="0.0")
    updated_date = models.DateTimeField(default=now)

    def __str__(self):
        return f'Price of {self.product_id} is  {self.prices} on {self.updated_date}'
