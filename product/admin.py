from django.contrib import admin
from .models import Product, Region, Product_Price_History
# Register your models here.


# Register your models here.
admin.site.register(Product)
admin.site.register(Region)
admin.site.register(Product_Price_History)

