from django.shortcuts import render
from django.db.models import Q
from product.serializers import (
    RegionSerializer,
    ProductSerializer,
    Product_PriceUpdate_Serializer,
)
from product.models import (
    Region,
    Product,
    Product_Price_History,
)
from rest_framework.generics import (
    CreateAPIView,
    ListAPIView,
    ListCreateAPIView,
    RetrieveDestroyAPIView,
    RetrieveUpdateDestroyAPIView,
)
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.permissions import AllowAny
from django_filters.rest_framework import DjangoFilterBackend


class RegionCreateListApiView(ListCreateAPIView):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer

class RegionUpdateDeleteDetailApiView(RetrieveUpdateDestroyAPIView):
    serializer_class = RegionSerializer
    queryset = Region.objects.all()
    lookup_field = 'id'


class ProductCreateApiView(CreateAPIView):
    serializer_class = ProductSerializer
    def post(self, request):
        try:
            got_data = request.data
            prd = Product.objects.create(
                name=got_data['name'],
                price=got_data['price'],
                qty=got_data['qty'],
                regionId_id=got_data['regionId'],
                description=got_data['description'],
                image1=got_data['image1'],
                image2=got_data['image2'],
                image3=got_data['image3'],
                inStock=got_data['inStock'],
            )
            prd_id = prd.id
            Product_Price_History.objects.create(
            product_id_id = prd_id, # _id :- to set foriegn key using integer
            prices = got_data['price']
            )
            return Response({"msg":"Product Created", "Product id":prd_id}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"Exceptions":str(e)}, status=HTTP_400_BAD_REQUEST)


class ProductListApiView(ListAPIView):
    '''
    List all created objects
    '''
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = '__all__'       



class ProductDeleteDetailApiView(RetrieveDestroyAPIView):
    '''
    Check details of a particular product.
    Delete a particular product
    '''
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'id'

class ProductPriceUpdateApiView(APIView):
    '''
    Update Product
    '''
    def put(self, request, id):
        try:
            prd_id = id
            got_data = request.data
            try:
                Product_Price_History.objects.create(
                product_id_id = prd_id,
                prices = got_data['price']
                )
                Product.objects.filter(id=prd_id).update(
                    name=got_data['name'],
                    price=got_data['price'],
                    qty=got_data['qty'],
                    regionId=got_data['regionId'],
                    description=got_data['description'],
                    image1=got_data['image1'],
                    image2=got_data['image2'],
                    image3=got_data['image3'],
                    inStock=got_data['inStock'],
                )
                return Response({'msg':'Product Updated Successfully!'}, status=HTTP_200_OK)
            except Product_Price_History.DoesNotExist as d: # if DoesNotExist exception occurs, then price history is created and then product is updated
                Product_Price_History.objects.create(
                product_id_id = prd_id,
                prices = got_data['price']
                )
                Product.objects.filter(id=prd_id).update(
                    name=got_data['name'],
                    price=got_data['price'],
                    qty=got_data['qty'],
                    regionId=got_data['regionId'],
                    description=got_data['description'],
                    image1=got_data['image1'],
                    image2=got_data['image2'],
                    image3=got_data['image3'],
                    inStock=got_data['inStock'],
                )
                return Response({'msg':"Product Updated Successfully!"}, status=HTTP_200_OK)
        except Exception as e:
            return Response({'failed':str(e)}, status=HTTP_400_BAD_REQUEST)


# Create your views here.
class ProductPriceListById(APIView):
    def get(self, request, id=None):
        try:
            received = Product_Price_History.objects.filter(product_id=id)
            return Response({"msg":"Price history of a object", "Prices":Product_PriceUpdate_Serializer(received, many=True).data}, status=HTTP_200_OK)
        except Exception as e:
            return Response({"msg":str(e)})
