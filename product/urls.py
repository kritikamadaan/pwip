from django.conf.urls import url
from .import views

urlpatterns = [
    url(r'^region_create_list/$', views.RegionCreateListApiView.as_view()),
    url(r'^region_detail/(?P<id>[0-9]+)/$', views.RegionUpdateDeleteDetailApiView.as_view()),  
    url(r'^product_create/$', views.ProductCreateApiView.as_view()),
    url(r'^product_list/$', views.ProductListApiView.as_view()),
    url(r'^product_detail/(?P<id>[0-9]+)/$', views.ProductDeleteDetailApiView.as_view()),
    url(r'^product_update/(?P<id>[0-9]+)/$', views.ProductPriceUpdateApiView.as_view()),
    url(r'^price_history/(?P<id>[0-9]+)/$', views.ProductPriceListById.as_view()),
]
