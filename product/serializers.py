from rest_framework import serializers
from product.models import (
    Region, 
    Product,
    Product_Price_History
)


# Create your serializers here

class RegionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Region
        fields = '__all__' # all means all fields will be serialized

class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = '__all__'

class Product_PriceUpdate_Serializer(serializers.ModelSerializer):

    class Meta:
        model = Product_Price_History
        fields = '__all__'
